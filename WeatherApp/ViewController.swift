//
//  ViewController.swift
//  WeatherApp
//
//  Created by Kirill Letko on 1/18/20.
//  Copyright © 2020 Letko. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

    let locationManager = CLLocationManager()
    var weatherData = WeatherData()
    
    
    @IBOutlet var cityName: UILabel!
    @IBOutlet var weatherImage: UIImageView!
    @IBOutlet var currentTemp: UILabel!
    @IBOutlet var weatherDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startLocationManager()
        // Do any additional setup after loading the view.
    }
    
    func startLocationManager() {
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.pausesLocationUpdatesAutomatically = false
            locationManager.startUpdatingLocation()
        }
    }
    
    func updateWeatherInfo(latitude: Double, longtitude: Double, handler: @escaping(_ weatherData: WeatherData?, _ error: Error?)->()) {
        let session = URLSession.shared
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?lat=\(latitude.description)&lon=\(longtitude.description)&units=metric&lang=en&APPID=586f233f5b8a6bf6b1252b568666e2ba") else {
            return
        }
        let task = session.dataTask(with: url) { (data, response, error) in
            guard error == nil else {
                print("\(error!.localizedDescription)")
                return
            }
            
            do {
                self.weatherData = try JSONDecoder().decode(WeatherData.self, from: data!)
                handler(self.weatherData, nil)
            } catch let error{
                handler(nil, error)
        }
        }
        task.resume()

    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lastLocation = locations.last {
            manager.stopUpdatingLocation()
            updateWeatherInfo(latitude: lastLocation.coordinate.latitude, longtitude: lastLocation.coordinate.longitude, handler: {(weatherData, error) in
               
                DispatchQueue.main.async {
                self.cityName.text = weatherData?.name
                    self.weatherImage.image = UIImage(named:(weatherData?.weather.last!.icon)!)
                self.currentTemp.text = weatherData?.main.temp.description
                self.weatherDescription.text = weatherData?.weather.last?.description
                }
        })
    }
}
}
