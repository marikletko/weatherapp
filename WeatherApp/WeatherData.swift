//
//  WeatherData.swift
//  WeatherApp
//
//  Created by Kirill Letko on 1/19/20.
//  Copyright © 2020 Letko. All rights reserved.
//

import Foundation

struct Weather: Decodable {
    var id: Int
    var main: String
    var description: String
    var icon: String
}

struct Main: Decodable {
    var temp: Double = 0.0
    var pressure: Int = 0
    var humidity: Int = 0
}

struct WeatherData: Decodable {
    var weather: [Weather] = []
    var main: Main = Main()
    var name: String = ""
}
